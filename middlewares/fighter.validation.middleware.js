const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    if(req.body.name === undefined ||
       req.body.power === undefined) {
           res.status(400).json({
               error: true,
               message: 'empty field not allowed'
           });
           
           return 'error';
       }
    if(req.body.power > 99 || req.body.power < 1 ||
        !Number.isInteger(req.body.power)) {
        res.status(400).json({
            error: true,
            message: 'power must be between -1 and 100'
        });
        return 'error';
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    const params = {
        "name": "",
        "health": 100,
        "power": 0,
        "defense": 1,
    }

    if(!Object.keys(req.query).every(e => {
        return Object.keys(params).includes(e);
    })) {
        res.status(400).json({
            error: true,
            message: 'property not found'
        });
        return 'error';
    }

    if(!Object.values(req.query).every(e => {
        return e.length > 0;
    })) {
        res.status(400).json({
            error: true,
            message: 'empty values not allowed'
        });
        return 'error';
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;