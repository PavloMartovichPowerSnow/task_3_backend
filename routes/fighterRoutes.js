const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', function(req, res){

    if(FighterService.getAllFighters()) {
        res.status(200).json(FighterService.getAllFighters());
    } else {
        res.status(404).json({
            error: true,
            message: 'page not found'
        });
    }
});

router.get('/:id', function(req, res) {
    res.status(200).send(FighterService.getFighter(req.params).name);
});

router.post('/', function(req, res, next) {

    const { name, power } = req.body;

    if(createFighterValid(req, res, next) !== 'error' &&
        responseMiddleware(req, res, next) !== 'error'){

        FighterService.createNewFighter(req.body);
        res.status(200).json({name: name, power: power});
    }
    //FighterService.deleteAllFighters();
});

router.put('/:id', function(req, res, next) {

    if(updateFighterValid(req, res, next) !== 'error' &&
       FighterService.getFighter(req.params)) {
        res.status(200).json({
            message: FighterService.updateFighter(req.params.id, req.query)
        });
    } else {
        res.status(404).json({
            error: true,
            message: 'user not found'
        });
    }

});

router.delete('/:id', function(req, res) {

    if(FighterService.getFighter(req.params)) {
        FighterService.deleteFighter(req.params.id);
        res.status(200).json({
            message: 'fighter was deleted'
        })
    } else {
        res.status(404).json({
            error: true,
            message: 'user not found'
        });
    }

});

module.exports = router;