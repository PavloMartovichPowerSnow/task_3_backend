const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', function(req, res){

    res.status(200).send('1');

});

router.get('/:id', function(req, res) {

    if(UserService.search(req.params)) {
        res.status(200).send(UserService.search(req.params).firstName);
    } else {
        res.status(404).json({
            message: 'user not found'
        });
    }

});

router.post('/', function(req, res, next) {

    const { firstName, lastName, email, phoneNumber } = req.body;

    if(createUserValid(req, res, next) !== 'error' &&
       responseMiddleware(req, res, next) !== 'error') {
        UserService.createNewUser({firstName, lastName, email, phoneNumber});

        res.status(200).json({
            firstName: firstName,
            lastName: lastName,
            email: email,
            phoneNumber: phoneNumber
        });
        
    }
    
    //UserService.deleteAllUsers();
});

router.put('/:id', function(req, res, next) {
    if(responseMiddleware(req, res, next) !== 'error' &&
       !UserService.getUser(req.params)) {
           UserService.updateUser(req.params.id, req.query)
       } else {
        
       }
});

router.delete('/:id', function(req, res) {

    if(UserService.getUser(req.params)) {
        UserService.deleteUser(req.params.id);
        res.status(200).json({
            message: 'user was deleted'
        });
    } else {
        res.status(404).json({
            error: true,
            message: 'user not found'
        });
    }
});

module.exports = router;