const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getUser(param) {
        return UserRepository.getOne(param);
    }
    createNewUser(data) {
        return UserRepository.create(data);
    }
    updateUser(userId, newData) {
        return UserRepository.update(userId, newData);
    }
    deleteUser(userId) {
        return UserRepository.delete(userId);
    }
    deleteAllUsers() {
        while(UserRepository.getAll().length > 0){
            this.deleteUser(UserRepository.getAll()[0]['id']);
        }
    }
    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    getAllUsers() {
        return UserRepository.getAll();
    }
}

module.exports = new UserService();